window.addEventListener("load",function(){
	alert("La pagina se ha cargado");

	var titulo = document.createElement("title");
	titulo.innerHTML = "Tarea 01 - 1016153 - TABLA";
	document.getElementsByTagName('head')[0].appendChild(titulo);
	
	var b = document.getElementById("body1");
	var h1 = document.createElement("h1");
	h1.innerHTML = "Entidades de México";
	b.appendChild(h1);
	
	var tabla = document.createElement("table");
	tabla.setAttribute("id", "tabla1");
	b.appendChild(tabla);
	
	var arrayEntidad = new Array("Distrito Federal","Aguascalientes","Baja California","Baja California Sur","Campeche","Chiapas","Chihuahua","Coahuila de Zaragoza","Colima","Durango","Guanajuato","Guerrero","Hidalgo","Jalisco","Mexico","Michoacan de Ocampo","Morelos","Nayarit","Nuevo Leon","Oaxaca","Puebla","Queretaro de Artega","Quintana Roo","San Luis Potosi","Sinaloa","Sonora","Tabasco","Tamaulipas","Tlaxcala","Veracruz de Ignacio de la Llave","Yucatan","Zacatecas");
	
	var t1 = document.getElementById("tabla1");
		var lrow = t1.rows.length;
		var row = t1.insertRow(lrow);
		
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);
		
		cell1.innerHTML="#";
		cell2.innerHTML="Entidad Federativa";
	
	for (var x=0; x<32; x=x+1){
		var lrow = t1.rows.length;
		var row = t1.insertRow(lrow);
		
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);
		
		cell1.innerHTML=(x+1)+"";
		cell2.innerHTML=arrayEntidad[x];
	}
});