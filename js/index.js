window.addEventListener("load",function(){
	alert("La pagina se ha cargado");

	var titulo = document.createElement("title");
	titulo.innerHTML = "Tarea 01 - 10161553";
	document.getElementsByTagName('head')[0].appendChild(titulo);

	var b = document.getElementById('body1');
	var h1 = document.createElement("h1");
	h1.innerHTML = "Herramientas de Programación Web (DSG1201)";
	b.appendChild(h1);
	
	var p = document.createElement("p");
	var s = document.createElement("strong");
	s.innerHTML = "10161553 - Vasquez Perez Luis Alberto";
	p.appendChild(s);
	b.appendChild(p);
	
	var h2 = document.createElement("h2");
	h2.innerHTML = "Tarea 01";
	b.appendChild(h2);
	
	var ul = document.createElement("ul");
	var a1 = document.createElement("a");
	a1.innerHTML = "Tabla de Entidades";
	a1.setAttribute("href", "./tabla.html")
	var a2 = document.createElement("a");
	a2.innerHTML = "Formulario de Captura";
	a2.setAttribute("href", "./formulario.html");
	ul.appendChild(a1);	
	var br = document.createElement("br");
	ul.appendChild(br);
	ul.appendChild(a2);
	b.appendChild(ul);
});